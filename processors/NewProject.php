<?php


namespace DEVCLI;


use League\CLImate\CLImate;

class NewProject extends Helper implements ProcessorInterface {

  public static function process(CLImate $climate, $response) {
    $input = $climate->input('Project Name: ');
    $project_name = $input->prompt();

    $site_available_path = "/usr/local/etc/nginx/sites-available/{$project_name}";

    // todo: git pull locahost-base
    Helper::run("cp /usr/local/etc/nginx/sites-available/localhost-base {$site_available_path}");
    Helper::run("replace \"[project]\" \"{$project_name}\" -- {$site_available_path}");
    Helper::run("ln -s {$site_available_path} /usr/local/etc/nginx/sites-enabled/.");
    Helper::run("echo '127.0.0.1 {$project_name}' >> /etc/hosts");
//
    Helper::run("nginx -s reload");

    // Print new project url
//    $climate->green("run sudo nano {$site_available_path}");
    $climate->green("Done.");
  }

}