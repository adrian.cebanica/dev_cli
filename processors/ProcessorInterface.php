<?php


namespace DEVCLI;

use League\CLImate\CLImate;

interface ProcessorInterface {

  public static function process(CLImate $climate, $response);

}