<?php


namespace DEVCLI;


use League\CLImate\CLImate;

class NewLemp extends Helper implements ProcessorInterface {


  public static function process(CLImate $climate, $response) {
//    $climate->out('YES');

    Helper::run('xcode-select --install');

//    Helper::run("osascript -e 'display notification \"Lorem ipsum dolor sit amet\" with title \"Title\"'");

//    Helper::run("brew install zsh-completions");
//    Helper::run("chmod go-w '/usr/local/share'");
//    Helper::run("brew install zsh zsh-completions");
//    Helper::run('sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"');

//    nano .zshrc
//    source ~/.bash_profile
//    export PATH="/usr/local/opt/icu4c/bin:$PATH"
//    export PATH="/usr/local/opt/icu4c/sbin:$PATH"

//    brew tap homebrew/homebrew-php
//    brew unlink php71 (If need to remove old version)
//    brew install php73
//    brew install php73-xdebug
//    brew install php73-imagick
//    brew install php73-redis
//    brew install php73-memcache
//    brew install Memcached

//    sudo nano /usr/local/etc/php/7.3/php-fpm.d/www.conf
//    Change the line: listen = 127.0.0.1:9000
//    to
//    listen = 127.0.0.1:9003

//    pecl install xdebug
//    sudo nano /usr/local/etc/php/7.3/php.ini
//    zend_extension=xdebug.so
//      [XDebug]
//    xdebug.remote_enable=1
//    xdebug.remote_autostart=1
//    xdebug.remote_handler=dbgp
//    xdebug.remote_mode=req
//    xdebug.remote_host=127.0.0.1
//    xdebug.remote_port=9000
//    xdebug.idekey=PHPSTORM

//    brew install MariaDB
//    ln -sfv /usr/local/opt/mariadb/*.plist ~/Library/LaunchAgent

//    Install PHP Myadmin to easily help manage Database
//    brew install phpmyadmin
//    Enable phpmyadmin in NGINX
//    Set the log path name in nginx to use your username
//
//    sudo nano /usr/local/etc/nginx/sites-available/phpmyadmin
//    change line:
//
//    error_log /User/[your username]/PHPstormProjects/_logs/phpmyadmin_error.log;
//    Enable config in Nginx
//
//    sudo ln -s /usr/local/etc/nginx/sites-available/phpmyadmin /usr/local/etc/nginx/sites-enabled/.
//    Set phpmyadmin as a host entry
//
//    sudo echo '127.0.0.1       phpmyadmin' > "etc/hosts"

//    brew install nginx
//    Add it to the Launch Agents to be started when starting up the system:
//
//    sudo cp /usr/local/opt/nginx/*.plist /Library/LaunchDaemons
//    Now launch it:
//
//    launchctl load -w /Library/LaunchDaemons/homebrew.mxcl.nginx.plist
//    Edit this section
//    Setup the config files
//    Create log directory where all your Nginx log file will be saved.
//
//    mkdir ~/PhpstormProjects/_logs
//    Download our base config for NGINX from GitLab and add it to our config.
//
//    git clone ssh://git@git.openimagination.co.uk:1974/ubuntu-systems/macios-native.git nginx-conf
//    sudo rm -rf /usr/local/etc/nginx && mv nginx-conf/nginx /usr/local/etc/nginx
//    sudo chown -R *[username]*:admin /usr/local/etc/nginx
//    Restart Nginx
//
//    sudo nginx -s reload

//    Drush
//    Setting up the Drush Launcher
//
//    When running "drush" from terminal it looks to see if the application exists in the /usr/bin directory. So we need to add drush to this location but in this case, we will trick the terminal by thinking the Drush Launcher is drush. Drush launcher will then point the drush commands to the drush version installed for each website via the composer installation.
//
//        We'll download the launcher then move it to the correct location. (This is based on using version 0.6.0)
//
//    curl -OL https://github.com/drush-ops/drush-launcher/releases/download/0.6.0/drush.phar
//    Rename the download to make it look like drush and move it to the bin directory:
//
//    sudo mv drush.phar /usr/local/bin/drush
//    Make downloaded file executable:
//
//    chmod +x /usr/local/bin/drush

//    Install it with Homebrew on MacOS
//
//    brew update && brew install mailhog
//    Edit the php.ini file so that Mailhog can pickup all emails sent from PHP
//
//    sudo nano /usr/local/etc/php/7.3/php.ini
//        ;sendmail_path =
//        sendmail_path = "/usr/local/opt/mailhog/bin/MailHog sendmail adrian@domain.co.uk"
//
//    nano ~/.bash_profile
//  alias nginx.start='sudo launchctl load /Library/LaunchDaemons/homebrew.mxcl.nginx.plist'
//  alias nginx.stop='sudo launchctl unload /Library/LaunchDaemons/homebrew.mxcl.nginx.plist'
//  alias nginx.restart='nginx.stop && nginx.start'
//
//  alias nginx1.stop='sudo launchctl unload /usr/local/opt/nginx/homebrew.mxcl.nginx.plist'
//
//  #alias php-fpm.start='launchctl load -w ~/Library/LaunchAgents/homebrew.mxcl.php@7.2.plist'
//  #alias php-fpm.stop='launchctl unload -w ~/Library/LaunchAgents/homebrew.mxcl.php@7.2.plist'
//  #alias php-fpm.restart='php-fpm.stop && php-fpm.start'
//
//  alias php-fpm.start='brew services start php'
//  alias php-fpm.stop='brew services stop php'
//  alias php-fpm.restart='php-fpm.stop && php-fpm.start'
//
//  alias mysql.start='launchctl load -w ~/Library/LaunchAgents/homebrew.mxcl.mariadb.plist'
//  alias mysql.stop='launchctl unload -w ~/Library/LaunchAgents/homebrew.mxcl.mariadb.plist'
//  alias mysql.restart='mysql.stop && mysql.start'
//
//  alias solr.stop=‘brew services stop solr’
//  alias solr.start=‘brew services start solr’
//  alias solr.restart=‘brew services restart solr’
//
//  alias nginx.logs.error='tail -250f /usr/local/etc/nginx/logs/error.log'
//  alias nginx.logs.access='tail -250f /usr/local/etc/nginx/logs/access.log'
//  alias nginx.logs.default.access='tail -250f /usr/local/etc/nginx/logs/default.access.log'
//  alias nginx.logs.default-ssl.access='tail -250f /usr/local/etc/nginx/logs/default-ssl.access.log'
//  alias nginx.logs.phpmyadmin.error='tail -250f /usr/local/etc/nginx/logs/phpmyadmin.error.log'
//  alias nginx.logs.phpmyadmin.access='tail -250f /usr/local/etc/nginx/logs/phpmyadmin.access.log'
  }

}