<?php


namespace DEVCLI;


use League\CLImate\CLImate;

class ImportSqlDump extends Helper implements ProcessorInterface {

  public static function process(CLImate $climate, $response) {
    $db_list = Helper::run("mysql -u drupal --password=drupal -e 'show databases;'", TRUE);
    $db_list = preg_split("/[\s,]+/", $db_list);
    $db_list = array_filter($db_list);
    $db_list = array_diff($db_list, ['Database', 'mysql', 'performance_schema', 'information_schema']);

    $input = $climate->radio('Select your DB: ', $db_list);
    $selected_db = $input->prompt();

    $path = '/Users/adrian/PhpstormProjects/sql_dump';
    $sql_files = array_diff(scandir($path), ['.', '..']);
    $sql_files = array_diff($sql_files, ['.DS_Store']);

    $input = $climate->radio('Select your sql file: ', $sql_files);
    $selected_sql = $input->prompt();

    Helper::run("mysql -u drupal --password=drupal $selected_db < $path/$selected_sql");
    $climate->green('Done.');

  }

}