<?php

use League\CLImate\CLImate;
use DEVCLI\MainMenu;

$loader = require_once __DIR__ . "/vendor/autoload.php";

$climate = new CLImate;

$input = $climate->radio('Select one of the following:', array_keys(MainMenu::$OPTIONS));
$response = $input->prompt();

if (posix_getuid() == 0) {
  if (array_key_exists($response, MainMenu::$OPTIONS)) {
    $handler = new MainMenu::$OPTIONS[$response];
    $handler::process($climate, $response);
  }
}
else {
  $climate->red('Run the script as sudo!');
}