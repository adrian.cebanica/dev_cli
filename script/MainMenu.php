<?php


namespace DEVCLI;


class MainMenu {

  public static $OPTIONS = [
//    'Install LEMP' => NewLemp::class,
    'Create new Project' => NewProject::class,
//    'Enable Project' => EnableProject::class,
//    'Disable Project' => DisableProject::class,
//    'Open log files' => OpenLogFile::class,
    'Import SQL Dump' => ImportSqlDump::class,
    '<< Back' => NavigateBack::class,
  ];

}